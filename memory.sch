EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title ""
Date "2021-06-30"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L highfinesse-psu-spi-rescue:IS42S16400J-xC-Memory_RAM U6
U 1 1 60DCE6E4
P 6200 3650
AR Path="/60DCDE1E/60DCE6E4" Ref="U6"  Part="1" 
AR Path="/60F1F935/60DCE6E4" Ref="U?"  Part="1" 
AR Path="/60DCE6E4" Ref="U?"  Part="1" 
AR Path="/60F9924B/60DCE6E4" Ref="U?"  Part="1" 
F 0 "U6" H 6200 3650 50  0000 C CNN
F 1 "IS42S16400J-xC" H 6950 2400 50  0000 C CNN
F 2 "Package_SO:TSOP-II-54_22.2x10.16mm_P0.8mm" H 6200 2250 50  0001 C CNN
F 3 "http://www.issi.com/WW/pdf/42-45S16400J.pdf" H 5600 4900 50  0001 C CNN
	1    6200 3650
	1    0    0    -1  
$EndComp
Text Label 6800 3950 0    50   ~ 0
D15
Text Label 6800 3850 0    50   ~ 0
D14
Text Label 6800 3750 0    50   ~ 0
D13
Text Label 6800 3650 0    50   ~ 0
D12
Text Label 6800 3550 0    50   ~ 0
D11
Text Label 6800 3450 0    50   ~ 0
D10
Text Label 6800 3350 0    50   ~ 0
D9
Text Label 6800 3250 0    50   ~ 0
D8
Text Label 6800 2850 0    50   ~ 0
D7
Text Label 6800 2950 0    50   ~ 0
D6
Text Label 6800 3050 0    50   ~ 0
D5
Text Label 6800 3150 0    50   ~ 0
D4
Text Label 6800 2750 0    50   ~ 0
D3
Text Label 6800 2650 0    50   ~ 0
D2
Text Label 6800 2550 0    50   ~ 0
D1
Text Label 6800 2450 0    50   ~ 0
D0
Entry Wire Line
	7200 2350 7100 2450
Entry Wire Line
	7200 2450 7100 2550
Entry Wire Line
	7200 2550 7100 2650
Entry Wire Line
	7200 2650 7100 2750
Entry Wire Line
	7200 2750 7100 2850
Entry Wire Line
	7200 2850 7100 2950
Entry Wire Line
	7200 2950 7100 3050
Entry Wire Line
	7200 3050 7100 3150
Entry Wire Line
	7200 3150 7100 3250
Entry Wire Line
	7200 3250 7100 3350
Entry Wire Line
	7200 3350 7100 3450
Entry Wire Line
	7200 3450 7100 3550
Entry Wire Line
	7200 3550 7100 3650
Entry Wire Line
	7200 3650 7100 3750
Entry Wire Line
	7200 3750 7100 3850
Entry Wire Line
	7200 3850 7100 3950
Wire Wire Line
	6700 2450 7100 2450
Wire Wire Line
	6700 2550 7100 2550
Wire Wire Line
	6700 2650 7100 2650
Wire Wire Line
	6700 2750 7100 2750
Wire Wire Line
	6700 2850 7100 2850
Wire Wire Line
	6700 2950 7100 2950
Wire Wire Line
	6700 3050 7100 3050
Wire Wire Line
	6700 3150 7100 3150
Wire Wire Line
	6700 3250 7100 3250
Wire Wire Line
	6700 3350 7100 3350
Wire Wire Line
	6700 3450 7100 3450
Wire Wire Line
	6700 3550 7100 3550
Wire Wire Line
	6700 3650 7100 3650
Wire Wire Line
	6700 3750 7100 3750
Wire Wire Line
	6700 3850 7100 3850
Wire Wire Line
	6700 3950 7100 3950
Text Label 5450 2450 0    50   ~ 0
BA0
Text Label 5450 2550 0    50   ~ 0
BA1
Text Label 5450 2750 0    50   ~ 0
A0
Text Label 5450 2850 0    50   ~ 0
A1
Text Label 5450 2950 0    50   ~ 0
A2
Text Label 5450 3050 0    50   ~ 0
A3
Text Label 5450 3150 0    50   ~ 0
A4
Text Label 5450 3250 0    50   ~ 0
A5
Text Label 5450 3350 0    50   ~ 0
A6
Text Label 5450 3450 0    50   ~ 0
A7
Text Label 5450 3550 0    50   ~ 0
A8
Text Label 5450 3650 0    50   ~ 0
A9
Text Label 5450 3750 0    50   ~ 0
A10
Text Label 5450 3850 0    50   ~ 0
A11
Entry Wire Line
	5250 2350 5350 2450
Entry Wire Line
	5250 2450 5350 2550
Entry Wire Line
	5150 2650 5250 2750
Entry Wire Line
	5150 2750 5250 2850
Entry Wire Line
	5150 2850 5250 2950
Entry Wire Line
	5150 2950 5250 3050
Entry Wire Line
	5150 3050 5250 3150
Entry Wire Line
	5150 3150 5250 3250
Entry Wire Line
	5150 3250 5250 3350
Entry Wire Line
	5150 3350 5250 3450
Entry Wire Line
	5150 3450 5250 3550
Entry Wire Line
	5150 3550 5250 3650
Entry Wire Line
	5150 3650 5250 3750
Entry Wire Line
	5150 3750 5250 3850
Wire Wire Line
	5350 2450 5700 2450
Wire Wire Line
	5350 2550 5700 2550
Wire Wire Line
	5250 2750 5700 2750
Wire Wire Line
	5250 2850 5700 2850
Wire Wire Line
	5250 2950 5700 2950
Wire Wire Line
	5250 3050 5700 3050
Wire Wire Line
	5250 3150 5700 3150
Wire Wire Line
	5250 3250 5700 3250
Wire Wire Line
	5250 3350 5700 3350
Wire Wire Line
	5250 3450 5700 3450
Wire Wire Line
	5250 3550 5700 3550
Wire Wire Line
	5250 3650 5700 3650
Wire Wire Line
	5250 3750 5700 3750
Wire Wire Line
	5250 3850 5700 3850
Wire Bus Line
	7200 1500 4500 1500
Wire Bus Line
	5250 1600 4500 1600
Wire Bus Line
	5150 1700 4500 1700
Text HLabel 4500 1500 0    50   BiDi ~ 0
D[0..15]
Text HLabel 4500 1600 0    50   Input ~ 0
BA[0..1]
Text HLabel 4500 1700 0    50   Input ~ 0
A[0..11]
Text HLabel 4500 4150 0    50   Input ~ 0
~CS
Text HLabel 4500 4250 0    50   Input ~ 0
CKE
Text HLabel 4500 4350 0    50   Input ~ 0
CLK
Text HLabel 4500 4450 0    50   Input ~ 0
LDQM
Text HLabel 4500 4550 0    50   Input ~ 0
UDQM
Text HLabel 4500 4650 0    50   Input ~ 0
~WE
Text HLabel 4500 4750 0    50   Input ~ 0
~CAS
Text HLabel 4500 4850 0    50   Input ~ 0
~RAS
Wire Wire Line
	4500 4150 5700 4150
Wire Wire Line
	4500 4250 5700 4250
Wire Wire Line
	4500 4350 5700 4350
Wire Wire Line
	4500 4450 5700 4450
Wire Wire Line
	4500 4550 5700 4550
Wire Wire Line
	4500 4650 5700 4650
Wire Wire Line
	4500 4750 5700 4750
Wire Wire Line
	4500 4850 5700 4850
$Comp
L power:+3V3 #PWR055
U 1 1 60DFEF1F
P 6200 2000
AR Path="/60DCDE1E/60DFEF1F" Ref="#PWR055"  Part="1" 
AR Path="/60F1F935/60DFEF1F" Ref="#PWR?"  Part="1" 
AR Path="/60F9924B/60DFEF1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR055" H 6200 1850 50  0001 C CNN
F 1 "+3V3" H 6215 2173 50  0000 C CNN
F 2 "" H 6200 2000 50  0001 C CNN
F 3 "" H 6200 2000 50  0001 C CNN
	1    6200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2000 6200 2100
Wire Wire Line
	6200 2100 6100 2100
Wire Wire Line
	6100 2100 6100 2250
Connection ~ 6200 2100
Wire Wire Line
	6200 2100 6200 2250
$Comp
L power:GND #PWR056
U 1 1 60E02FFB
P 6200 5250
AR Path="/60DCDE1E/60E02FFB" Ref="#PWR056"  Part="1" 
AR Path="/60F1F935/60E02FFB" Ref="#PWR?"  Part="1" 
AR Path="/60F9924B/60E02FFB" Ref="#PWR?"  Part="1" 
F 0 "#PWR056" H 6200 5000 50  0001 C CNN
F 1 "GND" H 6205 5077 50  0000 C CNN
F 2 "" H 6200 5250 50  0001 C CNN
F 3 "" H 6200 5250 50  0001 C CNN
	1    6200 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5250 6200 5150
Wire Wire Line
	6100 5050 6100 5150
Wire Wire Line
	6100 5150 6200 5150
Connection ~ 6200 5150
Wire Wire Line
	6200 5150 6200 5050
Wire Wire Line
	3000 6600 3000 6500
Wire Wire Line
	3000 6500 2500 6500
Wire Wire Line
	2500 7000 3000 7000
Wire Wire Line
	3000 7000 3000 6900
$Comp
L Device:C C?
U 1 1 60E0C465
P 3000 6750
AR Path="/60DB72DD/60E0C465" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C465" Ref="C41"  Part="1" 
AR Path="/60F1F935/60E0C465" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C465" Ref="C?"  Part="1" 
F 0 "C41" H 3115 6796 50  0000 L CNN
F 1 "100n" H 3115 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3038 6600 50  0001 C CNN
F 3 "~" H 3000 6750 50  0001 C CNN
	1    3000 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6600 3500 6500
Wire Wire Line
	3500 6500 3000 6500
Wire Wire Line
	3000 7000 3500 7000
Wire Wire Line
	3500 7000 3500 6900
$Comp
L Device:C C?
U 1 1 60E0C46F
P 3500 6750
AR Path="/60DB72DD/60E0C46F" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C46F" Ref="C42"  Part="1" 
AR Path="/60F1F935/60E0C46F" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C46F" Ref="C?"  Part="1" 
F 0 "C42" H 3615 6796 50  0000 L CNN
F 1 "100n" H 3615 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3538 6600 50  0001 C CNN
F 3 "~" H 3500 6750 50  0001 C CNN
	1    3500 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 6600 2500 6500
Wire Wire Line
	2500 6500 2000 6500
Wire Wire Line
	2000 7000 2500 7000
Wire Wire Line
	2500 7000 2500 6900
$Comp
L Device:C C?
U 1 1 60E0C479
P 2500 6750
AR Path="/60DB72DD/60E0C479" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C479" Ref="C40"  Part="1" 
AR Path="/60F1F935/60E0C479" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C479" Ref="C?"  Part="1" 
F 0 "C40" H 2615 6796 50  0000 L CNN
F 1 "100n" H 2615 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2538 6600 50  0001 C CNN
F 3 "~" H 2500 6750 50  0001 C CNN
	1    2500 6750
	1    0    0    -1  
$EndComp
Connection ~ 3000 6500
Connection ~ 3000 7000
$Comp
L Device:C C?
U 1 1 60E0C481
P 1000 6750
AR Path="/60DB72DD/60E0C481" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C481" Ref="C37"  Part="1" 
AR Path="/60F1F935/60E0C481" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C481" Ref="C?"  Part="1" 
F 0 "C37" H 1115 6796 50  0000 L CNN
F 1 "4u7" H 1115 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1038 6600 50  0001 C CNN
F 3 "~" H 1000 6750 50  0001 C CNN
	1    1000 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6600 1500 6500
Wire Wire Line
	1500 6500 1000 6500
Wire Wire Line
	1000 7000 1500 7000
Wire Wire Line
	1500 7000 1500 6900
$Comp
L Device:C C?
U 1 1 60E0C48B
P 1500 6750
AR Path="/60DB72DD/60E0C48B" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C48B" Ref="C38"  Part="1" 
AR Path="/60F1F935/60E0C48B" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C48B" Ref="C?"  Part="1" 
F 0 "C38" H 1615 6796 50  0000 L CNN
F 1 "100n" H 1615 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1538 6600 50  0001 C CNN
F 3 "~" H 1500 6750 50  0001 C CNN
	1    1500 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6600 2000 6500
Wire Wire Line
	2000 6500 1500 6500
Wire Wire Line
	1500 7000 2000 7000
Wire Wire Line
	2000 7000 2000 6900
$Comp
L Device:C C?
U 1 1 60E0C495
P 2000 6750
AR Path="/60DB72DD/60E0C495" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C495" Ref="C39"  Part="1" 
AR Path="/60F1F935/60E0C495" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C495" Ref="C?"  Part="1" 
F 0 "C39" H 2115 6796 50  0000 L CNN
F 1 "100n" H 2115 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2038 6600 50  0001 C CNN
F 3 "~" H 2000 6750 50  0001 C CNN
	1    2000 6750
	1    0    0    -1  
$EndComp
Connection ~ 1500 6500
Connection ~ 1500 7000
Connection ~ 2000 6500
Connection ~ 2000 7000
Wire Wire Line
	4000 6600 4000 6500
Wire Wire Line
	4000 6500 3500 6500
Wire Wire Line
	3500 7000 4000 7000
Wire Wire Line
	4000 7000 4000 6900
$Comp
L Device:C C?
U 1 1 60E0C4A3
P 4000 6750
AR Path="/60DB72DD/60E0C4A3" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E0C4A3" Ref="C43"  Part="1" 
AR Path="/60F1F935/60E0C4A3" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E0C4A3" Ref="C?"  Part="1" 
F 0 "C43" H 4115 6796 50  0000 L CNN
F 1 "100n" H 4115 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4038 6600 50  0001 C CNN
F 3 "~" H 4000 6750 50  0001 C CNN
	1    4000 6750
	1    0    0    -1  
$EndComp
Connection ~ 2500 6500
Connection ~ 2500 7000
Connection ~ 3500 6500
Connection ~ 3500 7000
Wire Wire Line
	1000 6600 1000 6500
Wire Wire Line
	1000 6900 1000 7000
$Comp
L power:+3V3 #PWR057
U 1 1 60E1A07B
P 1000 6400
AR Path="/60DCDE1E/60E1A07B" Ref="#PWR057"  Part="1" 
AR Path="/60F1F935/60E1A07B" Ref="#PWR?"  Part="1" 
AR Path="/60F9924B/60E1A07B" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 1000 6250 50  0001 C CNN
F 1 "+3V3" H 1015 6573 50  0000 C CNN
F 2 "" H 1000 6400 50  0001 C CNN
F 3 "" H 1000 6400 50  0001 C CNN
	1    1000 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 6400 1000 6500
Connection ~ 1000 6500
$Comp
L power:GND #PWR058
U 1 1 60E1BFBB
P 1000 7100
AR Path="/60DCDE1E/60E1BFBB" Ref="#PWR058"  Part="1" 
AR Path="/60F1F935/60E1BFBB" Ref="#PWR?"  Part="1" 
AR Path="/60F9924B/60E1BFBB" Ref="#PWR?"  Part="1" 
F 0 "#PWR058" H 1000 6850 50  0001 C CNN
F 1 "GND" H 1005 6927 50  0000 C CNN
F 2 "" H 1000 7100 50  0001 C CNN
F 3 "" H 1000 7100 50  0001 C CNN
	1    1000 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 7100 1000 7000
Connection ~ 1000 7000
Wire Wire Line
	4500 6600 4500 6500
Wire Wire Line
	4500 6500 4000 6500
Wire Wire Line
	4000 7000 4500 7000
Wire Wire Line
	4500 7000 4500 6900
$Comp
L Device:C C?
U 1 1 60E30D8D
P 4500 6750
AR Path="/60DB72DD/60E30D8D" Ref="C?"  Part="1" 
AR Path="/60DCDE1E/60E30D8D" Ref="C44"  Part="1" 
AR Path="/60F1F935/60E30D8D" Ref="C?"  Part="1" 
AR Path="/60F9924B/60E30D8D" Ref="C?"  Part="1" 
F 0 "C44" H 4615 6796 50  0000 L CNN
F 1 "100n" H 4615 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4538 6600 50  0001 C CNN
F 3 "~" H 4500 6750 50  0001 C CNN
	1    4500 6750
	1    0    0    -1  
$EndComp
Connection ~ 4000 6500
Connection ~ 4000 7000
Wire Bus Line
	5250 1600 5250 2450
Wire Bus Line
	5150 1700 5150 3750
Wire Bus Line
	7200 1500 7200 3850
$EndSCHEMATC
