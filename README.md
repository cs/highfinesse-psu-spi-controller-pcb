# HighFinesse PSU control PCB
This repository contains the design files for a control board to program
the DAC in the digital control option of the HighFinesse power supplies.

The board has an ethernet input for communication with the microcontroller
and another RJ45 output for connection to the power supply.
Synchronisation with the experiment must be performed using trigger pulses
sent to the BNC input.

This board is completely untested as the main MCU chip was not deliverable
in time due to the global chip shortage. It will require minor
modifications to the [firmware][1], which is currently expecting a
modified version of the CamDDS board.

 [1]: https://gitlab.physik.uni-muenchen.de/cs/highfinesse-psu-spi-controller-firmware
