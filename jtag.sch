EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date "2021-06-30"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7550 3500 0    50   Output ~ 0
~RESET
Text HLabel 7550 3800 0    50   Output ~ 0
SWDCLK
Text HLabel 7550 3900 0    50   BiDi ~ 0
SWDIO
$Comp
L power:+3V3 #PWR059
U 1 1 6100F543
P 8500 2900
F 0 "#PWR059" H 8500 2750 50  0001 C CNN
F 1 "+3V3" H 8515 3073 50  0000 C CNN
F 2 "" H 8500 2900 50  0001 C CNN
F 3 "" H 8500 2900 50  0001 C CNN
	1    8500 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR061
U 1 1 6100FA03
P 8600 4800
F 0 "#PWR061" H 8600 4550 50  0001 C CNN
F 1 "GND" H 8605 4627 50  0000 C CNN
F 2 "" H 8600 4800 50  0001 C CNN
F 3 "" H 8600 4800 50  0001 C CNN
	1    8600 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4800 8600 4700
Wire Wire Line
	7550 3500 7900 3500
Wire Wire Line
	7900 3800 7550 3800
Wire Wire Line
	7900 3900 7550 3900
$Comp
L Connector:Conn_ARM_JTAG_SWD_20 J4
U 1 1 61CFDD07
P 8500 3900
F 0 "J4" H 7970 3946 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_20" H 7970 3855 50  0000 R CNN
F 2 "Connector_IDC:IDC-Header_2x10_P2.54mm_Vertical" H 8950 2850 50  0001 L TNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.dui0499b/DUI0499B_system_design_reference.pdf" V 8150 2650 50  0001 C CNN
	1    8500 3900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8500 2900 8500 3000
Wire Wire Line
	8500 3000 8600 3000
Wire Wire Line
	8600 3000 8600 3100
Connection ~ 8500 3000
Wire Wire Line
	8500 3000 8500 3100
$Comp
L power:+3V3 #PWR060
U 1 1 61D004B0
P 7800 3250
F 0 "#PWR060" H 7800 3100 50  0001 C CNN
F 1 "+3V3" H 7815 3423 50  0000 C CNN
F 2 "" H 7800 3250 50  0001 C CNN
F 3 "" H 7800 3250 50  0001 C CNN
	1    7800 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3250 7800 3400
Wire Wire Line
	7800 3400 7900 3400
Wire Wire Line
	7800 3400 7800 4100
Wire Wire Line
	7800 4100 7900 4100
Connection ~ 7800 3400
NoConn ~ 7900 3700
NoConn ~ 7900 4000
NoConn ~ 7900 4300
NoConn ~ 7900 4400
$EndSCHEMATC
